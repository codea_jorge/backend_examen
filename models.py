from peewee import *
import datetime

DATABASE = PostgresqlDatabase(
    'examen_toucan',  # Required by Peewee.
    user='postgres',  # Will be passed directly to psycopg2.
    password='loco20wey5',  # Ditto.
    host='localhost',  # Ditto.
)

class Task(Model):
    class Meta:
        database = DATABASE
        db_table = 'Tasks'

    title = CharField(unique=True, max_length=250)
    description = TextField(null = True)
    time_execution = IntegerField(null = True, default = 0)
    seconds = IntegerField(null = True, default = 0)
    hours = IntegerField(null = True, default = 0)
    minutes = IntegerField(null = True, default = 0)
    finished = BooleanField(default = False, null = True)
    created_at = DateTimeField(default = datetime.datetime.now(), null = True )

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'time_execution': self.time_execution,
            'seconds': self.seconds,
            'minutes': self.minutes,
            'hours': self.hours,
            'finished': self.finished }

    @classmethod
    def new(cls, title, description, time_execution, seconds, minutes, hours, finished):
        try:
            return cls.create(
                title = title,
                description = description,
                time_execution = time_execution,
                seconds = seconds,
                minutes = minutes,
                hours = hours,
                finished = finished
            )

        except IntegrityError:
            print('error de ntegridad')
            return None

def create_task():
    title = 'cortar manzanas'
    description = 'la tarea es para saber que hay que cortar manzanas'

    if not Task.select().where(Task.title == title):
        Task.create(title = title, description = description)

def initialize():
    DATABASE.connect()
    DATABASE.create_tables ( [Task], safe=True )
    #create_task()
    DATABASE.close()
