from flask import Flask
from flask import jsonify
from flask import g
from flask import abort
from flask import request

from models import initialize
from models import Task
from models import DATABASE

app = Flask(__name__)
PORT = 9000
DEBUG = True
rootUrl = '/examen/api/v1.0'

@app.before_request
def before_request():
    g.db = DATABASE
    g.db.connect()

@app.after_request
def after_request(request):
    g.db.close()
    return request

@app.errorhandler(404)
def not_found(error):
    return jsonify( generate_response( 404, error = 'Tarea no encontrada' ) )

@app.errorhandler(404)
def bad_request(error):
    return jsonify( generate_response( 404, error = 'Necesitas parametros' ) )

@app.errorhandler(422)
def unprocessable_entity(error):
    return jsonify( generate_response( 422, error = 'es un proceso erroneo' ) )

# retiorna todas la tareas
@app.route( rootUrl + '/tasks', methods=['GET'])
def get_tasks():
    tasks = Task.select()
    tasks = [ task.to_json() for task in tasks ]

    return jsonify( generate_response( data =  tasks ) )

# detalle de una tarea
@app.route( rootUrl + '/tasks/<int:task_id>', methods=['GET'] )
def get_task(task_id):
    task = try_task(task_id)
    return jsonify( generate_response( data = task.to_json() ) )

def try_task(task_id):
    try:
        return Task.get( Task.id == task_id)
    except Task.DoesNotExist:
        abort(404)
# crea una tarea
@app.route( rootUrl + '/tasks/', methods=['POST'])
def post_task():
    if not request.json:
        abort(400)

    title = request.json.get('title', '')
    description = request.json.get('description', '')
    time_execution = request.json.get('time_execution', '')
    seconds = request.json.get('seconds', '')
    minutes = request.json.get('minutes', '')
    hours = request.json.get('hours', '')
    finished = request.json.get('finished', '')


    task = Task.new(title, description, time_execution, seconds, minutes, hours, finished)
    if task is None:
        abort(422)
    return jsonify( generate_response( data = task.to_json() ) )

# actualiza una tarea
@app.route( rootUrl + '/tasks/<int:task_id>', methods=['PUT'])
def put_task(task_id):
    task = try_task(task_id)
    if not request.json:
        abort(400)

    task.title = request.json.get('title', task.title)
    task.description = request.json.get('description', task.description)
    time_execution =  request.json.get('time_execution', task.time_execution)
    seconds = request.json.get('seconds', task.seconds)
    minutes = request.json.get('minutes', task.minutes)
    hours = request.json.get('hours', task.hours)
    finished = request.json.get('finished', task.finished)

    if task.save(): #si no esta en la base de datos
        return jsonify(generate_response(data = task.to_json() ))
    else:
        abort(422)

# elimina una tarea
@app.route( rootUrl + '/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    task = try_task(task_id)
    if task.delete_instance():
        return jsonify( generate_response( data = {} ) )
    else:
        abort(422)

def generate_response(status = 200, data = None, error = None):
    return {
        'status': status,
        'data': data,
        'error': error
    }

if __name__ == '__main__':
    initialize()
    app.run(port = PORT, debug = DEBUG )
